﻿using System;
using UnityEngine;

public class RotatingObject : MonoBehaviour
{
    [SerializeField] private float _speed = 10.0f;
    private void Update()
    {
        transform.localEulerAngles += Vector3.up * (_speed * Time.deltaTime);
    }
}
