﻿using System;
using DG.Tweening;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    [SerializeField] private Transform[] _points;
    [SerializeField] private float _duration;

    private Vector3[] _path;

    private void Awake()
    {
        _path = new Vector3[_points.Length];
    }

    private void OnEnable()
    {
        MoveForward();
    }

    private void MoveForward()
    {
        for (int i = 0; i < _points.Length; i++)
        {
            _path[i] = _points[i].position;
        }

        transform.DOPath(_path, _duration, PathType.CatmullRom, PathMode.Full3D)
            .OnComplete(MoveBackward);
    }

    private void MoveBackward()
    {
        for (int i = 0, j = _path.Length - 1; i < _points.Length; i++, j--)
        {
            _path[i] = _points[j].position;
        }

        transform.DOPath(_path, _duration, PathType.CatmullRom, PathMode.Full3D)
            .OnComplete(MoveBackward);
    }
}
