﻿using Common.Signal;
using HedgehogTeam.EasyTouch;
using UnityEngine;

namespace Common
{
    public class TouchFather : MonoBehaviour
    {
        private Vector2 _touchPositionNormalized;
        private bool _isTouched;

        private void OnEnable()
        {
            SubscribeEvents(true);
        }

        private void OnDisable()
        {
            SubscribeEvents(false);
        }

        /// <summary>
        /// Subscribes to touch events
        /// </summary>
        /// <param name="subscribe">true = subscribe | false = unsubscribe</param>
        private void SubscribeEvents(bool subscribe)
        {
            EasyTouch.On_TouchDown -= TouchDown;
            EasyTouch.On_TouchUp -= TouchUp;
            EasyTouch.On_SwipeEnd -= SwipeEnd;

            if (!subscribe)
                return;

            EasyTouch.On_TouchDown += TouchDown;
            EasyTouch.On_TouchUp += TouchUp;
            EasyTouch.On_SwipeEnd += SwipeEnd;
        }

        private void TouchDown(Gesture gesture)
        {
            _touchPositionNormalized = gesture.NormalizedPosition();
            _touchPositionNormalized.x -= 0.5f;
            _touchPositionNormalized.y -= 0.2f;

            _isTouched = true;

            Signals.Get<TouchNormalizedSignal>().Dispatch(_touchPositionNormalized, _isTouched);
        }

        private void TouchUp(Gesture gesture)
        {
            _isTouched = false;

            Signals.Get<TouchNormalizedSignal>().Dispatch(_touchPositionNormalized, _isTouched);
        }

        private void SwipeEnd(Gesture gesture)
        {
            if(gesture.swipe == EasyTouch.SwipeDirection.Up)
                Signals.Get<SwipeSignal>().Dispatch();
        }
    }
}
