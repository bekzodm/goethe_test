﻿namespace Common.Signal
{
    public class PlayerStatesSignal : ASignal<PlayerStates>
    { }
}
