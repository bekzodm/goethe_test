﻿using UnityEngine;

namespace Common.Signal
{
    public class TouchNormalizedSignal : ASignal<Vector2, bool>
    { }

    public class SwipeSignal : ASignal
    { }
}
