﻿using UnityEngine;

namespace Common
{
    public class GameSceneFather : MonoBehaviour
    {
        [SerializeField] private Player _player;
        public Transform SavePoint;

        public static GameSceneFather Instance = null;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if(Instance == this)
                Destroy(gameObject);
        }

        public Player GetPlayer()
        {
            return _player;
        }
    }
}
