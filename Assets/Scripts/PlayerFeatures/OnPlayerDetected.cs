﻿using UnityEngine;
using UnityEngine.Events;

namespace PlayerFeatures
{
    public class OnPlayerDetected : MonoBehaviour
    {
        [SerializeField] private string _playerTag;
        public UnityEvent PlayerEnter;
        public UnityEvent PlayerStay;
        public UnityEvent PlayerExit;

        private void OnTriggerEnter(Collider other)
        {
            bool isPlayer = other.CompareTag(_playerTag);

            if (!isPlayer) return;

            PlayerEnter.Invoke();
        }

        private void OnTriggerStay(Collider other)
        {
            bool isPlayer = other.CompareTag(_playerTag);

            if (!isPlayer) return;

            PlayerStay.Invoke();
        }

        private void OnTriggerExit(Collider other)
        {
            bool isPlayer = other.CompareTag(_playerTag);

            if (!isPlayer) return;

            PlayerExit.Invoke();
        }

        private void OnCollisionEnter(Collision other)
        {
            bool isPlayer = other.gameObject.CompareTag(_playerTag);

            if (!isPlayer) return;

            PlayerEnter.Invoke();
        }

        private void OnCollisionExit(Collision other)
        {
            bool isPlayer = other.gameObject.CompareTag(_playerTag);

            if (!isPlayer) return;
            
            PlayerExit.Invoke();
        }
    }
}
