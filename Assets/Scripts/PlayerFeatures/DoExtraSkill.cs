﻿using Common.Signal;
using UnityEngine;

namespace PlayerFeatures
{
    public class DoExtraSkill : MonoBehaviour
    {
        [SerializeField] private PlayerStates _skillType;

        public void PerformSkill()
        {
            Signals.Get<PlayerStatesSignal>().Dispatch(_skillType);
        }
    }
}
