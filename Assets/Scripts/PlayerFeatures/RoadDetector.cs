﻿using System;
using Common.Signal;
using UnityEngine;

namespace PlayerFeatures
{
    public class RoadDetector : MonoBehaviour
    {
        [SerializeField] private string _roadTag = "Road";

        private void OnTriggerEnter(Collider other)
        {
            if(!other.CompareTag(_roadTag)) return;

            DispatchSignal();
        }

        private void OnCollisionEnter(Collision other)
        {
            if (!other.gameObject.CompareTag(_roadTag)) return;

            DispatchSignal();
        }

        private void DispatchSignal()
        {
            Signals.Get<OnRoadSignal>().Dispatch();
        }
    }
}
