﻿using Common;
using Common.Signal;
using UnityEngine;

namespace PlayerFeatures
{
    public class PlayerExtraSkills : MonoBehaviour
    {
        [SerializeField] private float _turboPower = 1000.0f;
        [SerializeField] private float _superJumpPower = 400.0f;
        private Player _player;

        private bool _canDoSkill;

        private void OnEnable()
        {
            Signals.Get<PlayerStatesSignal>().AddListener(DoExtraSkill);
        }

        private void OnDisable()
        {
            Signals.Get<PlayerStatesSignal>().RemoveListener(DoExtraSkill);
        }

        private void DoExtraSkill(PlayerStates skill)
        {
            _player = GameSceneFather.Instance.GetPlayer();
            _canDoSkill = _player != null;

            if (!_canDoSkill) return;

            switch (skill)
            {
                case PlayerStates.Turbo:
                    Turbo();
                    break;
                case PlayerStates.SuperJump:
                    SuperJump();
                    break;
                case PlayerStates.Broken:
                    Broken();
                    break;
            }
        }

        private void Turbo()
        {
            _player.PlayerRigid.AddForce(transform.forward * _turboPower);
        }

        private void SuperJump()
        {
            _player.JumpForce = _superJumpPower;
        }

        private void Broken()
        {
            _player.CurrentState = PlayerStates.Broken;
        }
    }
}
