﻿using System;
using Common;
using Common.Signal;
using DG.Tweening;
using UnityEngine;

public enum PlayerStates
{
    Idle,
    Forward,
    Jump,
    Turbo,
    SuperJump,
    Broken
}

public class Player : MonoBehaviour
{
    public Rigidbody PlayerRigid => _playerRigid;
    public Transform PlayerTransform => _playerTransform;

    private PlayerStates _currentState;
    public PlayerStates CurrentState
    {
        get => _currentState;
        set => StateChanged(value);
    }

    public float JumpForce
    {
        get => _force;
        set => _force = value;
    }

    [SerializeField] private Rigidbody _playerRigid;
    [SerializeField] private HingeJoint _hinge;
    [SerializeField] private Transform _playerTransform;
    [SerializeField] private float _speed = 1000;
    [SerializeField] private float _force = 600;

    private Rigidbody _boneRigidBody;
    private JointMotor _motor;
    private Vector2 _touchPosition;
    private float _defaultJumpForce;
    private int _maxJumpCount = 2;
    private bool _isTouched;
    private bool _broking;

    private Tween _rotateTween;

    private void Awake()
    {
        _boneRigidBody = _playerTransform.GetComponent<Rigidbody>();

        _defaultJumpForce = _force;

        _motor.force = 400.0f;
        _hinge.motor = _motor;

    }

    private void OnEnable()
    {
        Signals.Get<TouchNormalizedSignal>().AddListener(Move);
        Signals.Get<SwipeSignal>().AddListener(Jump);
        Signals.Get<OnRoadSignal>().AddListener(ResetJumpCount);
    }

    private void OnDisable()
    {
        Signals.Get<TouchNormalizedSignal>().RemoveListener(Move);
        Signals.Get<SwipeSignal>().RemoveListener(Jump);
        Signals.Get<OnRoadSignal>().RemoveListener(ResetJumpCount);
    }

    private void Move(Vector2 touchPosition, bool isTouched)
    {
        if(_currentState == PlayerStates.Broken) return;

        _touchPosition = touchPosition;

        _isTouched = isTouched;
        
        _hinge.useMotor = isTouched;

        Rotate();

        if (!_isTouched)
        {
            _currentState = PlayerStates.Idle;
            ResetRotate();
            return;
        }

        _currentState = PlayerStates.Forward;
        
        _motor = _hinge.motor;

        _motor.targetVelocity = _touchPosition.y * _speed;

        _hinge.motor = _motor;
    }

    private void Jump()
    {
        if(_touchPosition.y < 0.5f || _maxJumpCount < 1) return;

        _maxJumpCount--;

        _playerRigid.AddForce(Vector3.up * _force);

        _force = _defaultJumpForce;
    }

    private void Rotate()
    {
        Vector3 newRotate = Vector3.zero;
        newRotate.x = _touchPosition.y * 60.0f;
        newRotate.y = _touchPosition.x * 60.0f;

        _rotateTween?.Kill();

        _rotateTween = _playerTransform.DOLocalRotate(newRotate, 0.5f);
    }

    private void ResetRotate()
    {
        _rotateTween?.Kill();

        _rotateTween = _playerTransform.DOLocalRotate(Vector3.zero, 0.5f);
    }

    private void BrokePlayer()
    {
        if(_broking) return;

        _broking = true;
        _boneRigidBody.constraints = RigidbodyConstraints.None;
        Invoke(nameof(ResetPosition), 2.0f);
    }

    private void ResetPosition()
    {
        _broking = false;

        _currentState = PlayerStates.Idle;

        _boneRigidBody.constraints = RigidbodyConstraints.FreezeRotation;
        _playerTransform.localEulerAngles = Vector3.zero;

        _playerTransform.position = GameSceneFather.Instance.SavePoint.position;
    }

    private void ResetJumpCount()
    {
        _maxJumpCount = 2;
    }

    private void StateChanged(PlayerStates state)
    {
        _currentState = state;
        if (_currentState == PlayerStates.Broken)
            BrokePlayer();
    }

}
