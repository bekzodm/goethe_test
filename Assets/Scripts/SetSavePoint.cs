﻿using Common;
using UnityEngine;

public class SetSavePoint : MonoBehaviour
{
    [SerializeField] private Transform _savePoint;

    public void SetSavePosition()
    {
        GameSceneFather.Instance.SavePoint = _savePoint;
    }
}
